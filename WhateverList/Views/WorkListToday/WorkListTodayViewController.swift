//
//  WorkListTodayViewController.swift
//  PairProgramming
//
//  Created by Natalia González Junco on 6/09/21.
//

import UIKit

class WorkListTodayViewController: UIViewController {
    
    var viewModel: WorkListTodayViewModelProtocol?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        viewModelConfiguration()
        tableViewConfiguration()
    }
    
    private func setupUI() {
        view.backgroundColor = .white
        self.title = "Today"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        let rightButton = UIBarButtonItem(image: UIImage(systemName: "terminal.fill"), style: .plain, target: self, action: #selector(self.addButtonPressed))
        
        self.navigationItem.rightBarButtonItem = rightButton
        
    }
    
    @objc private func addButtonPressed() {
        
    }
    
    private func viewModelConfiguration() {
        viewModel = WorkListTodayViewModel()
        viewModel?.refreshTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        viewModel?.start()
    }
    
    private func tableViewConfiguration() {
        // UICONFIGURATIONS
        tableView.separatorStyle = .none
        // CONFIGURATIONS.
        let nib = UINib(nibName: WorkListTodayCell.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: WorkListTodayCell.reuseId)
        tableView.dataSource = self
        tableView.delegate = self
    }

}

extension WorkListTodayViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.taskCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WorkListTodayCell.reuseId) as? WorkListTodayCell,
              let tableViewModel = self.viewModel else {
            return UITableViewCell()
        }
        
        guard let task = tableViewModel.taskAt(index: indexPath.row) else {
            assertionFailure("El task deberia existir")
            return UITableViewCell()
        }
        
        let cellViewModel = WorkListTodayCellViewModel(task: task)
        cell.setup(viewModel: cellViewModel)
        
        return cell
    }

}

extension WorkListTodayViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80
    }
}

