//
//  WorkListTodayViewModel.swift
//  PairProgramming
//
//  Created by Natalia González Junco on 6/09/21.
//

import Foundation

protocol WorkListTodayViewModelProtocol: AnyObject {
    var refreshTableView: (()->Void)? { get set }
    var taskCount: Int { get }
    func taskAt(index: Int) -> Task?
    func start()
    
}

class WorkListTodayViewModel: WorkListTodayViewModelProtocol {
    private var tasks: [Task] = []
    
    var refreshTableView: (()->Void)?
    
    var taskCount: Int {
        tasks.count
    }
    
    func taskAt(index: Int) -> Task? {
        tasks[index]
    }
    
    func start() {
        let getTasks = MocksFactory.getTaskMocks(size: 7)
        self.tasks.append(contentsOf: getTasks)
        
        DispatchQueue.global().async { [weak self] in
            sleep(3)
            self?.refreshTableView?()
        }
    }
    
    
}
