//
//  Task.swift
//  PairProgramming
//
//  Created by Natalia González Junco on 6/09/21.
//

import Foundation

struct Task {
    
    var name: String
    var description: String
    var status: TaskStatus
    var initialDate: Date
    var finalDate: Date?
    var subTasks: [Task]?
}
